/* --------------------------------------------------------
Uses CodeKit compiler - http://incident57.com/codekit

IMPORTS
@codekit-prepend "modernizr.custom.js"; 
@codekit-prepend "jquery-1.8.3.min.js"; 
@codekit-prepend "jquery.hoverIntent-r6.min.js";
@codekit-prepend "jquery.accessibledrop-1.0.js";
@codekit-prepend "jquery.cj-swipe-min.js";

-------------------------------------------------------- */ 

jQuery(document).ready(function($) {

// Window resize functions ----------------------------------------------
	
	function pageSetup() { 
		
		// Mobile Nav var
		var mobile_nav_display = $('#mobile-nav').css('display');
		
		// If mobile view
		if (mobile_nav_display === 'block') {			
			$('body').removeClass('large-screen').addClass('small-screen');		
		} 
		// If desktop/tablet view
		if (mobile_nav_display === 'none') {
			$('body').removeClass('active-nav small-screen').addClass('large-screen');	
		}
		
	}
	
	pageSetup();
	
	jQuery(window).resize(function(){
		pageSetup();
	});	
	
	
	
// Toggle button for mobile nav slider -----------------------------------------
	
	// On click, show/hide menu
	$('#menu-button a').click(function(e) {
		e.preventDefault();
		$('body').toggleClass("active-nav");							
	});							
	
	// On swipe, show/hide menu
	// To do: use modernizer to detect touch device and fire only on that?
	$('#mobile-nav, #mobile-search').touchSwipe(function(swipe){
		if (swipe == 'right') $('body').addClass("active-nav");
		if (swipe == 'left') $('body').removeClass("active-nav");
	});
	
	
// Toggle button for mobile menu dropdown ----------------------------------------	
	
	// Button click function
	$('#main-menu-button a').click(function(e) {
		e.preventDefault();
		mobileMenuToggle();								
	});
	
	// Make menu active by default on homepage with 'home' class on body tag
	if ($('body').hasClass('homepage')){
		$('#mobile-main-menu').addClass('active-menu'); // shows menu
	}	
	
	// Mobile-main-menu slide up/down toggle
	
	/* Note:
		jQuery's slide up/down sets an inline style of display:none.
		I'm removing that and adding/removing 'active-menu' class.
		This lets me control the display of the menu on hompage and desktop layouts with CSS.
	*/ 
	
	function mobileMenuToggle() {
		
		if ($('#mobile-main-menu').hasClass('active-menu')) {
			
			$('#mobile-main-menu').slideUp(200, function() {
				$('#mobile-main-menu').removeClass('active-menu'); // hides menu
				$('#mobile-main-menu').removeAttr('style');
			});
			
		} else {
			
			$('#mobile-main-menu').slideDown(200, function() {
				$('#mobile-main-menu').addClass('active-menu'); // shows menu
				$('#mobile-main-menu').removeAttr('style');
			});
		
		}
			
	}
	
	
// Hover Intent -------------------------------------------------------------
	
	// Don't use HI on homepage since nav dropdown will be visible by default
	if ($('.homepage').length == 0){
		
		$('li.home').hoverIntent({
			sensitivity: 7,
			interval: 75, 
			over: drops_show, 
			timeout: 250, 
			out: drops_hide
		});
	}
	
	// Hover Intent Dropdown fx
	function drops_show(){ 
	var el = $(this);
		$('ul',$(this)).stop().slideDown(200, function() {
			$(el).addClass('hover'); 
		});
	}
	function drops_hide(){ 
	var el = $(this);
		$('ul',$(this)).slideUp(100, function() {
			$(el).removeClass('hover'); 
		});
	}
	
	
// Accessible drop downs ----------------------------------------
   
    $('li.home').accessibleDropDown();
    
    	
// IE7 fixes ----------------------------------------    
    
    // IE7 news items clear fixing on homepage
	$('.lt-ie8 .news-item.clear').before('<div class="break"></div>');
		
	
	
});