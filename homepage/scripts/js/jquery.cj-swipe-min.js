/*
* jQuery Mobile Framework 1.1.0 db342b1f315c282692791aa870455901fdb46a55
* http://jquerymobile.com
*
* Copyright 2011 (c) jQuery Project
* Dual licensed under the MIT or GPL Version 2 licenses.
* http://jquery.org/license
*
*//*
* Stripped the touch swipe logic from jQuery Mobile and turned it into this plugin
* Copyright 2012 (c) CodingJack - http://codecanyon.net/user/CodingJack
* Dual licensed under the MIT or GPL Version 2 licenses.
*//* USAGE

// listen both left and right signals, the String "left" or "right" will be passed as an argument to the callback
* $(element).touchSwipe(callback); 

// second parameter is optional and will invoke "event.stopImmediatePropagation()" 
// use this if you need to prevent other mouse events from firing on the same object when a swipe gesture is detected
* $(element).touchSwipe(callback, true);

// listen for only the left swipe event
* $(element).touchSwipeLeft(callback); 

// listen for only the right swipe event
* $(element).touchSwipeRight(callback); 

// unbind both left and right swipe events
* $(element).unbindSwipe(); 

// unbind only left swipe event
* $(element).unbindSwipeLeft(); 

// unbind only right swipe event
* $(element).unbindSwipeRight();
 

// SPECIAL NOTES 
* all methods return "this" for chaining
* before a plugin event is added, "unbind" is called first to make sure events are never erroneously duplicated
 
*/
;(function(e){function a(t){e(this).touchSwipeLeft(t).touchSwipeRight(t)}function f(t){var n=e(this);n.data("swipeLeft")||n.data("swipeLeft",t);n.data("swipeRight")||c(n)}function l(t){var n=e(this);n.data("swipeRight")||n.data("swipeRight",t);n.data("swipeLeft")||c(n)}function c(e){e.unbindSwipe(!0).bind(r,h)}function h(r){function m(e){l.unbind(n);a&&v&&v-a<u&&Math.abs(c-p)>i&&Math.abs(h-d)<s&&(c>p?l.data("swipeLeft")&&l.data("swipeLeft")("left"):l.data("swipeRight")&&l.data("swipeRight")("right"));a=v=null}function g(e){if(a){f=e.originalEvent.touches?e.originalEvent.touches[0]:e;v=(new Date).getTime();p=f.pageX;d=f.pageY;Math.abs(c-p)>o&&e.preventDefault()}}var a=(new Date).getTime(),f=r.originalEvent.touches?r.originalEvent.touches[0]:r,l=e(this).bind(n,g).one(t,m),c=f.pageX,h=f.pageY,p,d,v;l.data("stopPropagation")&&r.stopImmediatePropagation()}var t,n,r,i=30,s=75,o=10,u=1e3;if("ontouchend"in document){t="touchend.cj_swp";n="touchmove.cj_swp";r="touchstart.cj_swp"}else{t="mouseup.cj_swp";n="mousemove.cj_swp";r="mousedown.cj_swp"}e.fn.touchSwipe=function(e,t){t&&this.data("stopPropagation",!0);if(e)return this.each(a,[e])};e.fn.touchSwipeLeft=function(e,t){t&&this.data("stopPropagation",!0);if(e)return this.each(f,[e])};e.fn.touchSwipeRight=function(e,t){t&&this.data("stopPropagation",!0);if(e)return this.each(l,[e])};e.fn.unbindSwipeLeft=function(){this.removeData("swipeLeft");this.data("swipeRight")||this.unbindSwipe(!0)};e.fn.unbindSwipeRight=function(){this.removeData("swipeRight");this.data("swipeLeft")||this.unbindSwipe(!0)};e.fn.unbindSwipe=function(e){e||this.removeData("swipeLeft swipeRight stopPropagation");return this.unbind(r+" "+n+" "+t)}})(jQuery);