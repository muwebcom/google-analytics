/*! ACCESSIBLE DROPDOWN */
/*--------------------------------------------------------------
	
	Allows keyboard controlled dropdown and hover effects.
	
	* Site:			http://uablogs.missouri.edu/interface/2011/08/keyboard-accessible/
	* Author:		Josh Hughes, MU Web Communications
	
	CSS rules:
	
	* Add styles on anchors for 'a:hover' and 'a:focus'
	* Add styles on list items for 'li:focus' and 'li.focus'
	
	With Hover Intent:
	
	* 'ul li ul' needs to be set to 'display:none' and position adjusted on hover. 
	* Hover Intent animates the display of the 'ul'.
	* With accessible dropdown, 'ul li.hover ul' needs to be set to 'display:block!important' and override Hover Intent's inline styles.
	
	Call function
	
	// Accessible Drop Down ----------------------------------------
	$("#nav").accessibleDropDown();
	
---------------------------------------------------------------- */	


$.fn.accessibleDropDown = function ()
{
    var el = $(this);

    /* Make dropdown menus keyboard accessible */

    $("a", el).focus(function() {
        $(this).parents("li").addClass("hover");
    }).blur(function() {
        $(this).parents("li").removeClass("hover");
    });
}