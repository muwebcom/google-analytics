/**
 *
 * GA tracking code
 * @author Jason L Rollins
 * Last updated: 2014-08-16
 *
 */

/**
 * Creates a new instance of the Tracking methods
 * 
 * @class GATracking
 * @namespace GATracking
 * @this {GATracking}
 * @version 1.0.1
 *
 */
GATracking = function() {
		
    /**
     * @class GatherEventData
     * @param {object} objSettings JSON data that comes from the instatiation of the method.
     * @returns {object} Returns an object with values used for settings
     */
    function GatherEventData(objSettings) {

        /**
         * @var {object} objData The data object to return back to the calling class
         */
        var objData = {};

        objData.strMainSelector = (typeof(objSettings.mainSelector) !== undefined) ? objSettings.mainSelector : false;
        objData.boolTrackExit   = (typeof(objSettings.trackExit) === 'boolean') ? objSettings.trackExit : true;
        objData.trackTime = ((objSettings.trackTime !== undefined) && (typeof(objSettings.trackTime) === 'number')) ? objSettings.trackTime * 1000 : false;

        return objData;
        
    };
	
    /**
     * This function gathers together all the parameters to be used in the GA event tracker function
     * 
     * @class GatherGAEventParams
     * @param {object} jQueryObject jQuery object of focus and which all subsequent selectors would be built off.
     * @param {object} objEventSettings Settings passed from the 'eventData' object in the JSON
     * @returns {object|number} Returns an object containing each event item for the array or false if the jQueryObject value is not a jquery object.
     */
    function GatherGAEventParams(jQueryObject,objEventSettings) {

        if (jQueryObject.jquery){

            /**
             * @var {object} objEventParams The data object returned containing the values for the event tracker
             */
            var objEventParams = {};

            objCategory = (objEventSettings.eventCategory !== undefined) ? objEventSettings.eventCategory[0] : '';
            objAction   = (objEventSettings.eventAction   !== undefined) ? objEventSettings.eventAction[0]   : '';
            objLabel    = (objEventSettings.eventLabel    !== undefined) ? objEventSettings.eventLabel[0]    : '';
            objValue    = (objEventSettings.eventValue    !== undefined) ? objEventSettings.eventValue[0]    : '';

            /**
             * @property {string} objEventParams.Category The Category string.
             */
            objEventParams.Category = (objCategory.string !== undefined) ? objCategory.string : (objCategory.selector !== undefined) ? RunJQuerySelect(jQueryObject,objCategory) : undefined;
            /**
             * @property {string} objEventParams.Action The Action string.
             */
            objEventParams.Action   = (objAction.string   !== undefined) ? objAction.string   : (objAction.selector   !== undefined) ? RunJQuerySelect(jQueryObject,objAction)   : undefined;
            /**
             * @property {string} objEventParams.Label The Label string.
             */    
            objEventParams.Label    = (objLabel.string    !== undefined) ? objLabel.string	  : (objLabel.selector    !== undefined) ? RunJQuerySelect(jQueryObject,objLabel)    : undefined;
            
            /**
             * @property {number} objEventParams.Value The Value string.
             */
            objEventParams.Value    = (objValue.integer   !== undefined) ? objValue.integer   : (objValue.selector    !== undefined) ? RunJQuerySelect(jQueryObject,objValue)    : undefined;

            return objEventParams;

        } else {

            return false;

        }


    };
			
    /**
     * Creates an instance of the Event Tracker with the given args
     * 
     * @class Represents the instance of the Google event tracking
     *
     * @param {string} strCategory The value of Category
     * @param {string} strAction The value of Action
     * @param {string=} strLabel The value of Label (optional)
     * @param {integer=} intValue The value of Value (optional)
     * 
     */
    function FireEvent(strCategory,strAction,strLabel,intValue){

        strEventCat = (strCategory !== '') ? strCategory : false;
        strEventAct = (strAction   !== '') ? strAction : false;
        strEventLab = (strLabel    !== undefined) ? strLabel.toString() : null;
        intEventVal = ((intValue   !== undefined) && (typeof intValue !== 'number')) ? intValue : null;

        /**
         *  
         * @description Google Analytics. Adding on the array.
         */
        ga('send','event',strEventCat,strEventAct,strEventLab,intEventVal);
        //console.log('send' + ',' + 'event' + ',' + strEventCat + ',' + strEventAct + ',' + strEventLab + ',' + intEventVal);
    }
	
    /**
     * Returns the value of the selector passed, based on directives given from the JSON
     * 
     * @class Runs jQuery select statements and returns values.
     *
     * @param {object} objSelected jQuery object, passed from the event.
     * @param {object} objSelectDirectives json object of selector directives
     *
     * @return {(string|number)} The value of the selected object.
     */
    function RunJQuerySelect(objSelected,objSelectDirectives) {

        var strSelector = objSelectDirectives.selector;
        var strType = objSelectDirectives.selType;
        var strTypeSelector = objSelectDirectives.typeSelector;

        if (strType === 'switchback') {

            return jQuerySwitchback(objSelected,strSelector,objSelectDirectives);

        } else if (strType === 'position') {

            return JQueryPosition(objSelected,strSelector);

        } else if (strType === 'attribute') {

            return JQueryAttr(objSelected,strTypeSelector,strSelector);

        } else if (strType === 'input') {

            return JQueryInput(objSelected,strSelector);

        } else {

            return JQueryText(objSelected,strSelector);

        }

        /**
         * 
         * @param {object} jQueryObject The jQuery object to run selectors off.
         * @param {string} selector The basic select statement.
         * @param {object} directives The switchback settings. 
         * @returns {String}
         */
        function jQuerySwitchback(jQueryObject,selector,directives) {

            /**
             * @var
             * @type @exp;directives@pro;switchbackSelector
             */
            var switchSelector     = directives.switchbackSelector;
            /**
             * @var
             * @type @exp;directives@pro;switchbackSelType
             */
            var switchSelectorType = directives.switchbackSelType;
            /**
             * @var
             * @type @exp;directives@pro;switchbackTypeSelector
             */
            var switchTypeSelector = directives.switchbackTypeSelector;

            /** As long as the selector isn't empty, continue on. */
            if (switchSelector === '') {

                return 'Error: Must select the node to switchback to.';

            } else {
                
                if (switchSelectorType === 'attribute') {

                    return JQueryAttr(jQueryObject.parents(selector),switchTypeSelector,switchSelector);

                } else if (switchSelectorType === 'input') {

                    return JQueryInput(jQueryObject.parents(selector),switchSelector);

                } else if (switchSelectorType === 'position') {

                    return JQueryPosition(jQueryObject,selector,switchSelector);

                } else {

                    return JQueryText(jQueryObject.parents(selector),switchSelector);

                }

            }

        }
		
        /**
         * 
         * @param {type} jQueryObject The jQuery object to run selectors off.
         * @param {type} ancSelector The ancestor to point back to.
         * @returns {number} Returns an int of the position of the element selected.
         */
        function JQueryPosition(jQueryObject,ancSelector) {

            if (ancSelector === '') {

	            //alert(jQueryObject.index() + 1);
                return jQueryObject.index() + 1;

            } else {
            
	            //alert(jQueryObject.parents(ancSelector).index(ancSelector) + 1);
            	return jQueryObject.parents(ancSelector).index(ancSelector) + 1;

            }

        }
        
        /**
         * 
         * @param {object} jQueryObject The jQuery object to run selectors off.
         * @param {string} selector The jQuery select statement
         * @returns {string} Returns the value currently in the input field at time of selection.
         */
        function JQueryInput(jQueryObject,selector) {

            if (selector === '') {

                return undefined;

            } else {

                return jQueryObject.find(selector).val();

            }

        }

        /**
         * 
         * @param {object} jQueryObject The jQuery object to run selectors off.
         * @param {string} attrName The attribute to select
         * @param {string=} selector An additional selector to use in order to find a farther down attribute.
         * @returns {string} Returns the value of the attribute selected.
         */
        function JQueryAttr(jQueryObject,attrName,selector) {

            if (selector === '') {
            	
                return jQueryObject.attr(attrName);

            } else {
            
                return jQueryObject.find(selector).attr(attrName);

            }

        }
        
        /**
         * 
         * @param {object} jQueryObject The jQuery object to run selectors off.
         * @param {string=} selector An additional selector that allows farther down selection
         * @returns {string} Returns the text of the selected element
         */
        function JQueryText(jQueryObject,selector) {

            if (selector === '') {

                return jQueryObject.text();

            } else {

                return jQueryObject.find(selector).text();

            }

        }

    }
    
    /**
     * Simply tracks all outgoing links from a given page.
     * 
     * @function TrackExit
     * @memberOf GATracking
     * 
     */
    this.TrackExit = function(){
		
        // Click event
        jQuery('a').not('.ga-ignore').on('click.TrackExit',function(event) {

            if (!event.isDefaultPrevented()) {

                event.preventDefault();
                var objClickedLink = jQuery(this);
                var strCurrentDomain = document.domain;
                var strLinkDomain = this.hostname;
                var strCategory = 'Exit';
                var strAction = objClickedLink.text() !== '' ? objClickedLink.text() : 'Empty';
                var strLabel = objClickedLink.attr('href');

                if (strCurrentDomain !== strLinkDomain) {
                    FireEvent(strCategory, strAction, strLabel);
                    //console.log('Track Event: Category => ' + strCategory + '; Action => ' + strAction + '; ' + 'Label => ' + strLabel);
                }
                
                setTimeout(function(){window.location = objClickedLink.attr('href');},200);

            }

        });
						
    };
    
    /**
     * Accepts settings to choose a jQuery element and fire Event tracking on click.
     * 
     * @function TrackClickEvent
     * @memberOf GATracking
     * @param {object} jsonEventData json object with settings for the click event.
     * 
     */
    this.TrackClickEvent = function(jsonEventData) {

        /**
         * 
         * @var {object} Uses GatherEventData to return the formatted settings.
         */
        var objData = GatherEventData(jsonEventData);

        if (objData.strMainSelector !== undefined) {

            var i = 0, eventDirectives;
            /**
             * @todo Can we somehow provide a list of trackers to turn off?
             *
             */
            if (!objData.boolTrackExit) {

                jQuery(objData.strMainSelector).off('click.TrackExit');

            }

            for (i = 0; i < jsonEventData.eventData.length; i++) {

                eventDirectives = jsonEventData.eventData[i];

                // Click event
                jQuery(objData.strMainSelector).on('click.TrackClickEvent',function(event) {

                    event.preventDefault();
                    
                    /**
                     * 
                     * @var {object} Returns the event categories
                    */
                    var objEventParams = GatherGAEventParams(jQuery(this),eventDirectives);
                    FireEvent(objEventParams.Category, objEventParams.Action, objEventParams.Label, objEventParams.Value);
                    //console.log('Track Event: Category => ' + objEventParams.Category + '; Action => ' + objEventParams.Action + '; ' + 'Label => ' + objEventParams.Label + '; ' + 'Value => ' + objEventParams.Value);
                    
                    var strURL = jQuery(this).attr('href');
                    setTimeout(function(){window.location = strURL;},200);
                });

            }

        }

    };
	
    /**
     * Accepts settings to track a form submission. Can track inputs in conjunction with {@link jQueryInput}
     * 
     * @function TrackFormSubmit
     * @memberOf GATracking
     * @param {object} jsonEventData json object with settings for the click event.
     */
    this.TrackFormSubmit = function(jsonEventData) {
        
        /**
         * 
         * @var {object} Uses GatherEventData to return the formatted settings.
         */
        var objData = GatherEventData(jsonEventData);

        var eventDirectives = jsonEventData.eventData[0];

        jQuery(objData.strMainSelector).submit(function(event) {

            event.preventDefault();
            
            /**
             * 
             * @var {object} Returns the event categories
             */
            var objEventParams = GatherGAEventParams(jQuery(this),eventDirectives);
            FireEvent(objEventParams.Category, objEventParams.Action, objEventParams.Label, objEventParams.Value);
            //console.log('Track Event: Category => ' + strCategory + '; Action => ' + strAction + '; ' + 'Label => ' + strLabel + '; ' + 'Value => ' + intValue);

            strURLParams = createURLParams(event.currentTarget);
            setTimeout(function(){window.location = event.currentTarget.action + strURLParams;},200);

        });

        /**
         * 
         * @param {type} objInputs The form elements to parse
         * @returns {string} Returns the URL string to forward the form onto after event submission
         */
        function createURLParams(objInputs) {
            
            var strURLParams = '?';
            var intElementsLength = objInputs.elements.length;

            for (var i=0; i < intElementsLength; i++) {

                var strParamName = objInputs.elements[i].name;
                var strParamVal = encodeURI(objInputs.elements[i].value);
                var j = i + 1;

                if (strParamName !== undefined) {

                    strURLParams += (strParamName.length > 0) ? strParamName + '=' + strParamVal : '';
                    strURLParams += ((strParamName.length > 0) && objInputs.elements[j] !== undefined) ? '&' : '';

                }

            }

            return strURLParams;

        }

    };

    /**
     * Tracks the viewing of a specific element on a page. Can delay the event firing based on intFireDelay
     * 
     * @function TrackViewEvent
     * @memberOf GATracking
     * @param {type} jsonEventData
     */
    this.TrackViewEvent = function(jsonEventData) {

        /**
         * 
         * @var {object} Uses GatherEventData to return the formatted settings.
         */
        var objData = GatherEventData(jsonEventData);

        var boolViewStatus = false;
        var boolHasBeenViewed = false;

        var intFocusPoint = (jsonEventData.focusPoint !== undefined) ? jsonEventData.focusPoint : 0;
        var intFocusBubble = (jsonEventData.focusBubble !== undefined) ? jsonEventData.focusBubble : 25;

        var intFireDelay = (jsonEventData.fireDelay !== undefined) ? jsonEventData.fireDelay * 1000 : 0;
        var boolPersistentView = (jsonEventData.persistentView !== undefined) ? jsonEventData.persistentView : false;

        var intVieweeTop = Math.round(jQuery(objData.strMainSelector).offset().top);
        //var intVieweeHeight = Math.round(jQuery(objData.strMainSelector).height());

        try {

            var eventDirectives = jsonEventData.eventData[0];

            if (!objData.strMainSelector)                       throw 'You must define an element to select.';
            if (eventDirectives.eventCategory === undefined)    throw 'Missing event Category';
            if (eventDirectives.eventAction === undefined)      throw 'Missing event Action.';

            var readTimer;

            jQuery(window).scroll(function() {

                var intViewportTop = jQuery(window).scrollTop();
                var intViewportHeight = jQuery(window).height();
                //var intViewportBottom = intViewportTop + intViewportHeight;
                var intViewportFocus = Math.round(intViewportHeight * intFocusPoint) + intViewportTop;
                var intViewportFocusMin = intViewportFocus - intFocusBubble;
                var intViewportFocusMax = intViewportFocus + intFocusBubble;

                if (!boolHasBeenViewed && !boolViewStatus && (intVieweeTop <= intViewportFocusMax && intVieweeTop >= intViewportFocusMin)) {

                    boolViewStatus = true;

                    startTimer();

                } 

                if (boolPersistentView && !boolHasBeenViewed && boolViewStatus && (intVieweeTop > intViewportFocusMax || intVieweeTop < intViewportFocusMin)) {

                    boolViewStatus = false;	
                    stopTimer();

                    readTimer = undefined;
                }

                function startTimer() {

                    readTimer = setTimeout(function(){

                        /**
                         * 
                         * @var {object} Returns the event categories
                         */
                        var objEventParams = GatherGAEventParams(jQuery(this),eventDirectives);
                        FireEvent(objEventParams.Category, objEventParams.Action, objEventParams.Label, objEventParams.Value);
                        boolHasBeenViewed = true;
                        
                    }, intFireDelay);

                }

                function stopTimer() {

                    clearTimeout(readTimer);

                }

            });

        } catch (error) {

            //console.log(error);

        }

    };

    /**
     * Tracks the viewing of a specific element on a page. Can delay the event firing based on intFireDelay
     * 
     * @function TrackTime
     * @memberOf GATracking
     * @param {type} jsonEventData
     */
    this.TrackTime = function(jsonEventData) {

        /**
         * 
         * @var {object} Uses GatherEventData to return the formatted settings.
         */
        var objData = GatherEventData(jsonEventData);

        var eventDirectives = jsonEventData.eventData[0];
        var boolHasInitiated = false;

        jQuery(window).scroll(function() {

            if (!boolHasInitiated) {

                boolHasInitiated = true;

                viewTimer = setTimeout(function() {

                   /**
                    * 
                    * @var {object} Returns the event categories
                    */
                   var objEventParams = GatherGAEventParams(jQuery(this),eventDirectives);
                   FireEvent(objEventParams.Category, objEventParams.Action, objEventParams.Label, objEventParams.Value);
                        
                }, objData.trackTime);

            }

        });
        
    };
};
